# Instruction
1) `./gradlew runContainers` - to run containers
2) `./gradlew runTests` - to run tests

Allure server will be started by default

Swagger documentation will be started by default on http://localhost:9090/