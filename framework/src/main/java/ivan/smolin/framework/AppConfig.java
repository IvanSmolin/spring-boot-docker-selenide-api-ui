package ivan.smolin.framework;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.github.javafaker.Faker;
import ivan.smolin.framework.ui.authentication.resources.CreateAccountPageResources;
import ivan.smolin.framework.ui.authentication.resources.LoginPageResources;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;

import java.io.File;
import java.util.Locale;

@SpringBootApplication
public class AppConfig {
    private final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

    @Value("${app.localePath}")
    private String localePath;

    @Bean
    public TestRestTemplate testRestTemplate() {
        return new TestRestTemplate();
    }

    @Bean
    public Faker faker() {
        return new Faker(new Locale(System.getenv("LOCALE")));
    }

    @Bean
    public LoginPageResources loginPageResources() {
        return readYml(LoginPageResources.class);
    }

    @Bean
    public CreateAccountPageResources createAccountPageResources() {
        return readYml(CreateAccountPageResources.class);
    }

    @SneakyThrows
    private <T> T readYml(Class<T> valueType) {
        return mapper.readValue(
            new File(localePath + System.getenv("LOCALE") + "/" + valueType.getSimpleName() + ".yml"),
            valueType
        );
    }
}