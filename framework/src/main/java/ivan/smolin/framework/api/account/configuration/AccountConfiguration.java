package ivan.smolin.framework.api.account.configuration;

import com.zaxxer.hikari.HikariDataSource;
import ivan.smolin.framework.api.common.BaseConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableJpaRepositories(
    basePackages = "ivan.smolin.framework.api.account.repository",
    entityManagerFactoryRef = "accountEntityManager",
    transactionManagerRef = "accountTransactionManager"
)
public class AccountConfiguration extends BaseConfiguration {
    @Value("${datasource.account.url}")
    private String url;

    @Bean
    public HikariDataSource accountDataSource() {
        return dataSource(url);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean accountEntityManager() {
        return entityManagerFactoryBean(accountDataSource(), "ivan.smolin.framework.api.account.entity");
    }

    @Bean
    public PlatformTransactionManager accountTransactionManager() {
        return transactionManager(accountEntityManager());
    }
}