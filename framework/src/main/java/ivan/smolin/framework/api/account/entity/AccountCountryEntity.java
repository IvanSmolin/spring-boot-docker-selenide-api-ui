package ivan.smolin.framework.api.account.entity;

import ivan.smolin.framework.api.account.model.AccountCountryModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "account_country")
public final class AccountCountryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public AccountCountryModel toAccountCountryModel() {
        return new AccountCountryModel()
            .setId(this.id)
            .setName(this.name)
            .setCreatedAt(this.createdAt.toString());
    }
}