package ivan.smolin.framework.api.account.entity;

import ivan.smolin.framework.api.account.model.AccountExchangeRateModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "account_exchange_rate")
public final class AccountExchangeRateEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private Double rate;

    @Column(name = "rate_date")
    private LocalDate rateDate;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public AccountExchangeRateModel toAccountExchangeRateModel() {
        return new AccountExchangeRateModel()
            .setId(this.id)
            .setRate(this.rate)
            .setRateDate(this.rateDate.toString())
            .setCreatedAt(this.createdAt.toString());
    }
}