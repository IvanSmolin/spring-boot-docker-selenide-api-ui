package ivan.smolin.framework.api.account.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public final class AccountModel {
    private final boolean active = true;

    private Integer id;

    private String name;

    private List<AccountExchangeRateModel> exchangeRates;

    private AccountCountryModel country;

    private List<AccountExtraModel> extras;

    @JsonProperty("created_at")
    private String createdAt;
}