package ivan.smolin.framework.api.account.model;

import lombok.Data;

@Data
public final class AccountResponseModel {
    private int id;
}