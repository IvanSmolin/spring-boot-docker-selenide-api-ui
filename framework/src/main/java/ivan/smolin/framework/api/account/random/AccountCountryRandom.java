package ivan.smolin.framework.api.account.random;

import ivan.smolin.framework.api.account.entity.AccountCountryEntity;
import ivan.smolin.framework.api.account.model.AccountCountryModel;
import ivan.smolin.framework.api.common.CommonRandom;
import org.springframework.stereotype.Component;

import static ivan.smolin.framework.api.account.constants.AccountConstants.NAME_MAX_LENGTH;
import static ivan.smolin.framework.api.account.constants.AccountConstants.NAME_MIN_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MIN_NUMBER;

@Component
public final class AccountCountryRandom extends CommonRandom {
    /**
     * Метод для генерации Account Country Entity
     */
    public AccountCountryEntity getRandomEntity() {
        return new AccountCountryEntity()
            .setName(faker.name().name())
            .setCreatedAt(getDate());
    }

    /**
     * Методы для генерации Account Country Model
     */
    public AccountCountryModel getRandomModelMax() {
        return new AccountCountryModel()
            .setName(getString(NAME_MAX_LENGTH))
            .setCreatedAt(getStringDate());
    }

    public AccountCountryModel getRandomModelMin() {
        return new AccountCountryModel()
            .setName(getString(NAME_MIN_LENGTH))
            .setCreatedAt(getStringDate());
    }

    public AccountCountryModel getRandomModelExceeded() {
        return new AccountCountryModel()
            .setName(getString(NAME_MAX_LENGTH + MIN_NUMBER))
            .setCreatedAt(getStringDate());
    }

    public AccountCountryModel getRandomModelLessMin() {
        return new AccountCountryModel()
            .setName(getString(NAME_MIN_LENGTH - MIN_NUMBER))
            .setCreatedAt(getStringDate());
    }
}