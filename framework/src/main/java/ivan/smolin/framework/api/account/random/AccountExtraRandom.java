package ivan.smolin.framework.api.account.random;

import ivan.smolin.framework.api.account.entity.AccountExtraEntity;
import ivan.smolin.framework.api.account.model.AccountExtraModel;
import ivan.smolin.framework.api.common.CommonRandom;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MAX_LIST_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.ZERO_NUMBER;

@Component
public final class AccountExtraRandom extends CommonRandom {
    public List<AccountExtraEntity> getRandomListEntity() {
        return Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
            .limit(MAX_LIST_LENGTH)
            .map(it -> getRandomEntity())
            .collect(Collectors.toList());
    }

    public List<AccountExtraModel> getRandomListModel() {
        return Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
            .limit(MAX_LIST_LENGTH)
            .map(it -> getRandomModel())
            .collect(Collectors.toList());
    }

    /**
     * Метод для генерации Account Extra Entity
     */
    private AccountExtraEntity getRandomEntity() {
        return new AccountExtraEntity()
            .setName(faker.name().name())
            .setCreatedAt(getDate());
    }

    /**
     * Методы для генерации Account Extra Model
     */
    private AccountExtraModel getRandomModel() {
        return new AccountExtraModel()
            .setName(faker.name().name())
            .setCreatedAt(getStringDate());
    }
}