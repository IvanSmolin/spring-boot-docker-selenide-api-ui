package ivan.smolin.framework.api.account.repository;

import ivan.smolin.framework.api.account.entity.AccountEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends CrudRepository<AccountEntity, Integer> {
    @Query(
        value = "select * " +
            "from account " +
            "where active = true " +
            "order by random() " +
            "limit 1",
        nativeQuery = true
    )
    AccountEntity getRandomActiveAccount();

    @Query(
        "SELECT e " +
            "FROM AccountEntity e " +
            "WHERE e.active = true"
    )
    List<AccountEntity> getAllActiveEntities();

    List<AccountEntity> findAllByActiveTrue();

    @Query(
        "SELECT e " +
            "FROM AccountEntity e " +
            "WHERE e.name = ?1"
    )
    AccountEntity getByName(String id);

    AccountEntity findByName(String name);
}