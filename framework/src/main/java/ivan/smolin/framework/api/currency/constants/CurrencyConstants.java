package ivan.smolin.framework.api.currency.constants;

public final class CurrencyConstants {
    public static final String COUNTRY = "country";

    public static final String NAME = "name";

    public static final String EXCHANGE_RATES = "exchangeRates";

    public static final String COUNTRY_NAME = COUNTRY + "." + NAME;

    public static final int NAME_MAX_LENGTH = 100;

    public static final int NAME_MIN_LENGTH = 5;

    public static final int CURRENCY_NAME_MIN_LENGTH = 2;
}