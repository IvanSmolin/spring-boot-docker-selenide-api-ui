package ivan.smolin.framework.api.currency.entity;

import ivan.smolin.framework.api.currency.model.CurrencyCountryModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "currency_country")
public final class CurrencyCountryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public CurrencyCountryModel toCurrencyCountryModel() {
        return new CurrencyCountryModel()
            .setId(this.id)
            .setName(this.name)
            .setCreatedAt(this.createdAt.toString());
    }
}