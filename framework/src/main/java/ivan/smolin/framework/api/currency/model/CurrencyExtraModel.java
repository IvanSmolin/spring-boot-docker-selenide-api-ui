package ivan.smolin.framework.api.currency.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public final class CurrencyExtraModel {
    private Integer id;

    private String name;

    @JsonProperty("created_at")
    private String createdAt;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCreatedAt() {
        return createdAt;
    }
}