package ivan.smolin.framework.api.currency.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public final class CurrencyModel {
    private final boolean active = true;

    private Integer id;

    private String name;

    private List<CurrencyExchangeRateModel> exchangeRates;

    private CurrencyCountryModel country;

    private List<CurrencyExtraModel> extras;

    @JsonProperty("created_at")
    private String createdAt;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isActive() {
        return active;
    }

    public List<CurrencyExchangeRateModel> getExchangeRates() {
        return exchangeRates;
    }

    public CurrencyCountryModel getCountry() {
        return country;
    }

    public List<CurrencyExtraModel> getExtras() {
        return extras;
    }

    public String getCreatedAt() {
        return createdAt;
    }
}