package ivan.smolin.framework.api.currency.model;

import lombok.Data;

@Data
public final class CurrencyResponseModel {
    private int id;
}