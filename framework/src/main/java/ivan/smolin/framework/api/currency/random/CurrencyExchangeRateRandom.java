package ivan.smolin.framework.api.currency.random;

import ivan.smolin.framework.api.common.CommonRandom;
import ivan.smolin.framework.api.currency.entity.CurrencyExchangeRateEntity;
import ivan.smolin.framework.api.currency.model.CurrencyExchangeRateModel;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MAX_LIST_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.ZERO_NUMBER;

@Component
public final class CurrencyExchangeRateRandom extends CommonRandom {
    public List<CurrencyExchangeRateEntity> getRandomListEntity() {
        return Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
            .limit(MAX_LIST_LENGTH)
            .map(it -> getRandomEntity())
            .collect(Collectors.toList());
    }

    public List<CurrencyExchangeRateModel> getRandomListModel() {
        return Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
            .limit(MAX_LIST_LENGTH)
            .map(it -> getRandomModel())
            .collect(Collectors.toList());
    }

    /**
     * Метод для генерации Currency Exchange Rate Entity
     */
    private CurrencyExchangeRateEntity getRandomEntity() {
        return new CurrencyExchangeRateEntity()
            .setRate(getDouble())
            .setRateDate(LocalDate.now())
            .setCreatedAt(getDate());
    }

    /**
     * Методы для генерации Currency Exchange Rate Model
     */
    private CurrencyExchangeRateModel getRandomModel() {
        return new CurrencyExchangeRateModel()
            .setRate(getDouble())
            .setCreatedAt(getStringDate())
            .setRateDate(LocalDate.now().toString());
    }
}