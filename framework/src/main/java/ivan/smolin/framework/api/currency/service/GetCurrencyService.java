package ivan.smolin.framework.api.currency.service;

import ivan.smolin.framework.api.common.BaseService;
import ivan.smolin.framework.api.currency.model.CurrencyModel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public final class GetCurrencyService extends BaseService {
    public ResponseEntity<CurrencyModel> get(long id) {
        return getRequest(
            appProperties.getCurrencyBaseUrl() + "/" + id,
            CurrencyModel.class
        );
    }

    public ResponseEntity<String> getError(long id) {
        return getError(String.valueOf(id));
    }

    public ResponseEntity<String> getError(String id) {
        return getRequest(
            appProperties.getCurrencyBaseUrl() + "/" + id,
            String.class
        );
    }
}