package ivan.smolin.framework.api.currency.service;

import ivan.smolin.framework.api.common.BaseService;
import ivan.smolin.framework.api.currency.model.CurrencyModel;
import ivan.smolin.framework.api.currency.model.CurrencyResponseModel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public final class PostCurrencyService extends BaseService {
    public ResponseEntity<CurrencyResponseModel> post(CurrencyModel requestModel) {
        return postRequest(
            appProperties.getCurrencyBaseUrl(),
            requestModel,
            CurrencyResponseModel.class
        );
    }

    public ResponseEntity<String> postError(CurrencyModel requestModel) {
        return postRequest(
            appProperties.getCurrencyBaseUrl(),
            requestModel,
            String.class
        );
    }
}