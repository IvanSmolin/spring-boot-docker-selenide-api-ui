package ivan.smolin.framework.api.user.constants;

public final class UserEnumeration {
    public enum Title {
        MR("Mr."),
        MRS("Mrs.");

        private final String name;

        Title(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}