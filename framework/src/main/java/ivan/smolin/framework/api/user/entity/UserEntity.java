package ivan.smolin.framework.api.user.entity;

import ivan.smolin.framework.api.user.constants.UserEnumeration.Title;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "user")
public final class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String email;

    @Column
    private Title title;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String password;

    @Column
    private Date birthday;

    @Column
    private String addressFirstName;

    @Column
    private String addressLastName;

    @Column
    private String company;

    @Column
    private String address;

    @Column
    private String secondAddress;

    @Column
    private String city;

    @Column
    private String state;

    @Column
    private String zip;

    @Column
    private String country;

    @Column
    private String additionalInformation;

    @Column
    private String homePhone;

    @Column
    private String mobilePhone;

    @Column
    private String alias;
}