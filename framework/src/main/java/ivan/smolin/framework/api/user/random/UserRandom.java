package ivan.smolin.framework.api.user.random;

import ivan.smolin.framework.api.common.CommonRandom;
import ivan.smolin.framework.api.user.constants.UserEnumeration.Title;
import ivan.smolin.framework.api.user.entity.UserEntity;
import org.springframework.stereotype.Component;

@Component
public final class UserRandom extends CommonRandom {
    /**
     * Метод для генерации User Entity
     */
    public UserEntity getRandomEntity() {
        return new UserEntity()
            .setEmail(getEmail(10))
            .setFirstName(faker.name().firstName())
            .setLastName(faker.name().lastName())
            .setAddressFirstName(faker.address().firstName())
            .setAddressLastName(faker.address().lastName())
            .setHomePhone(faker.phoneNumber().cellPhone())
            .setMobilePhone(faker.phoneNumber().phoneNumber())
            .setTitle(getEnum(Title.class))
            .setPassword(getString(10))
            .setBirthday(faker.date().birthday())
            .setCompany(faker.company().name())
            .setAddress(faker.address().streetAddress())
            .setSecondAddress(faker.address().secondaryAddress())
            .setCity(faker.address().city())
            .setState(faker.address().state())
            .setZip(getNumberString(5))
            .setAdditionalInformation(getString(10))
            .setAlias(getString(10));
    }
}