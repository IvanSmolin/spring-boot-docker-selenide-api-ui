package ivan.smolin.framework.ui.authentication.module.createAccount;

import ivan.smolin.framework.ui.authentication.resources.CreateAccountPageResources;
import ivan.smolin.framework.ui.common.component.BaseComponent;
import ivan.smolin.framework.ui.common.component.Dropdown;
import ivan.smolin.framework.ui.common.component.TextArea;
import ivan.smolin.framework.ui.common.component.TextField;
import lombok.Getter;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

@Getter
public final class AddressModule extends BaseComponent {
    private final TextField firstName;

    private final TextField lastName;

    private final TextField company;

    private final TextField address;

    private final TextField secondAddress;

    private final TextField city;

    private final Dropdown state;

    private final TextField zip;

    private final Dropdown country;

    private final TextArea info;

    private final TextField homePhone;

    private final TextField mobilePhone;

    private final TextField alias;

    public AddressModule(CreateAccountPageResources createAccountPageResources) {
        String text = createAccountPageResources.getYourAddress();
        this.element = $(byText(text)).parent().as("Модуль \"" + text + "\"");

        this.firstName = new TextField(createAccountPageResources.getFirstName(), element);
        this.lastName = new TextField(createAccountPageResources.getLastName(), element);
        this.company = new TextField(createAccountPageResources.getCompany(), element);
        this.address = new TextField(createAccountPageResources.getAddress(), element);
        this.secondAddress = new TextField(createAccountPageResources.getSecondAddress(), element);
        this.city = new TextField(createAccountPageResources.getCity(), element);
        this.state = new Dropdown(createAccountPageResources.getState(), element);
        this.zip = new TextField(createAccountPageResources.getZip(), element);
        this.country = new Dropdown(createAccountPageResources.getCountry(), element);
        this.info = new TextArea(createAccountPageResources.getAdditionalInformation(), element);
        this.homePhone = new TextField(createAccountPageResources.getHomePhone(), element);
        this.mobilePhone = new TextField(createAccountPageResources.getMobilePhone(), element);
        this.alias = new TextField(createAccountPageResources.getAlias(), element);
    }
}