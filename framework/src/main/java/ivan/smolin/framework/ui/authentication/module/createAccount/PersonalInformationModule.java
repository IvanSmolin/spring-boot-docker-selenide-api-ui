package ivan.smolin.framework.ui.authentication.module.createAccount;

import ivan.smolin.framework.ui.authentication.resources.CreateAccountPageResources;
import ivan.smolin.framework.ui.authentication.resources.LoginPageResources;
import ivan.smolin.framework.ui.common.component.BaseComponent;
import ivan.smolin.framework.ui.common.component.RadioButton;
import ivan.smolin.framework.ui.common.component.TextField;
import lombok.Getter;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

@Getter
public final class PersonalInformationModule extends BaseComponent {
    private final RadioButton title;

    private final TextField firstName;

    private final TextField lastName;

    private final TextField email;

    private final TextField password;

    public PersonalInformationModule(
        LoginPageResources loginPageResources,
        CreateAccountPageResources createAccountPageResources
    ) {
        String text = createAccountPageResources.getPersonalInformation();
        this.element = $(byText(text)).parent().as("Модуль \"" + text + "\"");

        this.title = new RadioButton(createAccountPageResources.getTitle(), element);
        this.firstName = new TextField(createAccountPageResources.getFirstName(), element);
        this.lastName = new TextField(createAccountPageResources.getLastName(), element);
        this.email = new TextField(createAccountPageResources.getEmail(), element);
        this.password = new TextField(loginPageResources.getPassword(), element);
    }
}