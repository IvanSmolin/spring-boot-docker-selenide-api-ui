package ivan.smolin.framework.ui.authentication.module.login;

import ivan.smolin.framework.ui.authentication.resources.LoginPageResources;
import ivan.smolin.framework.ui.common.component.BaseComponent;
import ivan.smolin.framework.ui.common.component.Button;
import ivan.smolin.framework.ui.common.component.TextField;
import lombok.Getter;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

@Getter
public final class CreateAccountModule extends BaseComponent {
    private final TextField email;

    private final Button createAccount;

    public CreateAccountModule(LoginPageResources loginPageResources) {
        String text = loginPageResources.getCreateAccount();
        this.element = $(byText(text)).parent().as("Модуль \"" + text + "\"");

        this.email = new TextField(loginPageResources.getEmailAddress(), element);
        this.createAccount = new Button(loginPageResources.getCreateAccount(), element);
    }
}