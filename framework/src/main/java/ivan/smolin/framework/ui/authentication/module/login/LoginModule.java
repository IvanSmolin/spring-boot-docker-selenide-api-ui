package ivan.smolin.framework.ui.authentication.module.login;

import ivan.smolin.framework.ui.authentication.resources.LoginPageResources;
import ivan.smolin.framework.ui.common.component.BaseComponent;
import ivan.smolin.framework.ui.common.component.Button;
import ivan.smolin.framework.ui.common.component.TextField;
import lombok.Getter;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

@Getter
public final class LoginModule extends BaseComponent {
    private final TextField email;

    private final TextField password;

    private final Button signIn;

    public LoginModule(LoginPageResources loginPageResources) {
        String text = loginPageResources.getAlreadyRegistered();
        this.element = $(byText(text)).parent().as("Модуль \"" + text + "\"");

        this.email = new TextField(loginPageResources.getEmailAddress(), element);
        this.password = new TextField(loginPageResources.getPassword(), element);
        this.signIn = new Button(loginPageResources.getSignIn(), element);
    }
}