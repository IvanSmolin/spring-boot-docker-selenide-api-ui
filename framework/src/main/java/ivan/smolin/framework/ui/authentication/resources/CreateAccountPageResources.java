package ivan.smolin.framework.ui.authentication.resources;

import lombok.Getter;

@Getter
public final class CreateAccountPageResources {
    private String personalInformation;

    private String title;

    private String firstName;

    private String lastName;

    private String email;

    private String yourAddress;

    private String company;

    private String address;

    private String secondAddress;

    private String city;

    private String state;

    private String zip;

    private String country;

    private String additionalInformation;

    private String homePhone;

    private String mobilePhone;

    private String alias;

    private String register;
}