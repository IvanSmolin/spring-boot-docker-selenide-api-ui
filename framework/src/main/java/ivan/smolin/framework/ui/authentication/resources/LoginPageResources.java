package ivan.smolin.framework.ui.authentication.resources;

import lombok.Getter;

@Getter
public final class LoginPageResources {
    private String emailAddress;

    private String createAccount;

    private String password;

    private String signIn;

    private String alreadyRegistered;

    private String welcomeText;
}