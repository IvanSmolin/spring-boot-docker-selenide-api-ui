package ivan.smolin.framework.ui.common;

import com.codeborne.selenide.WebDriverProvider;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import javax.annotation.Nonnull;
import java.net.MalformedURLException;
import java.net.URL;

public final class CustomProvider implements WebDriverProvider {
    @Nonnull
    @Override
    public WebDriver createDriver(DesiredCapabilities capabilities) {

        try {
            URL url = new URL("http://localhost:4444/wd/hub");

            capabilities.setBrowserName(System.getenv("BROWSER"));
            if (capabilities.getBrowserName().equals("")) {
                capabilities.setBrowserName("chrome");
            }

            capabilities.setVersion(System.getenv("VERSION"));
            capabilities.setCapability("name", "Ivan Smolin");
            capabilities.setCapability("enableVNC", false);
            capabilities.setCapability("enableVideo", false);

            return new RemoteWebDriver(url, capabilities);
        } catch (WebDriverException exception) {
            if (exception.getMessage().contains("401 Unauthorized")) {
                throw new WebDriverException("Incorrect credentials for Selenoid");
            } else if (exception.getMessage().contains("unsupported browser")) {
                throw new WebDriverException("Unsupported browser version");
            } else {
                throw exception;
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Can not start selenium", e);
        }
    }
}