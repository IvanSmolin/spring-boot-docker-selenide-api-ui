package ivan.smolin.framework.ui.common.component;

import com.codeborne.selenide.SelenideElement;

public abstract class BaseComponent {
    protected SelenideElement element;

    protected String name;

    protected String getComponentAlias() {
        return this.getClass().getSimpleName().replaceAll("([^_])([A-Z])", "$1 $2") + " '" + name + "'";
    }
}