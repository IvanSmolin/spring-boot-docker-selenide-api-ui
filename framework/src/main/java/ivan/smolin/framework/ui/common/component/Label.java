package ivan.smolin.framework.ui.common.component;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public final class Label extends BaseComponent {
    public Label(String text) {
        this.name = text;
        this.element = $(byText(name)).as(getComponentAlias());
    }

    public void shouldBeVisible() {
        element.shouldBe(visible);
    }
}