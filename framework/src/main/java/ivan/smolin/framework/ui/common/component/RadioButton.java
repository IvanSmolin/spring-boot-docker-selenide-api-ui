package ivan.smolin.framework.ui.common.component;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public final class RadioButton extends BaseComponent {
    public RadioButton(String text) {
        this.name = text;
        this.element = getElement($(byText(name)));
    }

    public RadioButton(String text, SelenideElement element) {
        this.name = text;
        this.element = getElement(element.$(byText(name)));
    }

    public void selectRadioWithText(String text) {
        element.scrollTo().$(byText(text)).as(getComponentAlias() + ": value '" + text + "'").click();
    }

    private SelenideElement getElement(SelenideElement element) {
        return element.parent().as(getComponentAlias());
    }
}