package ivan.smolin.framework.api_tests.account;

import ivan.smolin.framework.api.account.random.AccountRandom;
import ivan.smolin.framework.api.account.repository.AccountRepository;
import ivan.smolin.framework.api.account.service.DeleteAccountService;
import ivan.smolin.framework.api.account.service.GetAccountService;
import ivan.smolin.framework.api.account.service.PostAccountService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MAX_LIST_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.ZERO_NUMBER;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class AccountApiBaseTestClass {
    /**
     * Repositories
     */
    @Autowired
    protected AccountRepository accountRepository;

    /**
     * Random Helpers
     */
    @Autowired
    protected AccountRandom accountRandom;

    /**
     * Services
     */
    @Autowired
    protected PostAccountService postAccountService;

    @Autowired
    protected GetAccountService getAccountService;

    @Autowired
    protected DeleteAccountService deleteAccountService;

    @BeforeAll
    public void initializeAccountDatabase() {
        accountRepository.saveAll(
            Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
                .limit(MAX_LIST_LENGTH)
                .map(it -> accountRandom.getRandomEntity())
                .collect(Collectors.toList())
        );
    }
}