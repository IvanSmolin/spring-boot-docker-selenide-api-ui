package ivan.smolin.framework.api_tests.account.delete_account;

import ivan.smolin.framework.api_tests.account.AccountApiBaseTestClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static ivan.smolin.framework.api.common.CustomHttpResponseAssertion.assertBadRequestStringResponse;
import static ivan.smolin.framework.api.common.CustomHttpResponseAssertion.assertNotFoundStringResponse;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.INTEGER_EXCEEDED_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.INTEGER_MAX_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MAX_URL_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.ZERO_NUMBER;

@DisplayName("API. Account. DELETE '/account/{id}'. Negative tests of account deletion by ID")
public final class NegativeTests extends AccountApiBaseTestClass {
    @Test
    @DisplayName("API. Account. DELETE '/account/{id}'. Unknown ID")
    public void testUnknownIdFailed() {
        var responseEntity = deleteAccountService.delete(INTEGER_MAX_LENGTH);
        assertNotFoundStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. DELETE '/account/{id}'. ID is greater than the boundary value for int type")
    public void testNotIntegerIdFailed() {
        var responseEntity = deleteAccountService.delete(INTEGER_EXCEEDED_LENGTH);
        assertBadRequestStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. DELETE '/account/{id}'. ID is zero")
    public void testZeroIdFailed() {
        var responseEntity = deleteAccountService.delete(ZERO_NUMBER);
        assertNotFoundStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. DELETE '/account/{id}'. ID - negative number")
    public void testNegativeIdFailed() {
        var responseEntity = deleteAccountService.delete(accountRandom.getNegativeNumber());
        assertNotFoundStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. DELETE '/account/{id}'. ID - non-numeric value with minimum length")
    public void testStringMinIdFailed() {
        var responseEntity = deleteAccountService.delete(accountRandom.getTextString(MIN_NUMBER));
        assertBadRequestStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. DELETE '/account/{id}'. ID - non-numeric value with maximum length")
    public void testStringMaxIdFailed() {
        var responseEntity = deleteAccountService.delete(accountRandom.getTextString(MAX_URL_LENGTH));
        assertBadRequestStringResponse(responseEntity);
    }
}