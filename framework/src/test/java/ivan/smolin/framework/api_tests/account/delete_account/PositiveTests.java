package ivan.smolin.framework.api_tests.account.delete_account;

import ivan.smolin.framework.api_tests.account.AccountApiBaseTestClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("API. Account. DELETE '/account/{id}'. Positive tests of account deletion by ID")
public final class PositiveTests extends AccountApiBaseTestClass {
    @Test
    @DisplayName("API. Account. DELETE '/account/{id}'. Deleting an account by ID")
    public void testSuccess() {
        var entity = accountRandom.getRandomEntity();
        accountRepository.save(entity);
        var responseEntity = deleteAccountService.delete(entity.getId());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}