package ivan.smolin.framework.api_tests.account.get_account;

import ivan.smolin.framework.api_tests.account.AccountApiBaseTestClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static ivan.smolin.framework.api.common.CustomHttpResponseAssertion.assertBadRequestResponse;
import static ivan.smolin.framework.api.common.CustomHttpResponseAssertion.assertNotFoundResponse;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.INTEGER_EXCEEDED_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.INTEGER_MAX_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MAX_URL_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.ZERO_NUMBER;

@DisplayName("API. Account. GET '/account/{id}'. Negative tests of getting an account by ID")
public final class NegativeTests extends AccountApiBaseTestClass {
    @Test
    @DisplayName("API. Account. GET '/account/{id}'. Unknown ID")
    public void testUnknownIdFailed() {
        var responseEntity = getAccountService.getError(INTEGER_MAX_LENGTH);
        assertNotFoundResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. GET '/account/{id}'. ID is greater than the boundary value for int type")
    public void testNotIntegerIdFailed() {
        var responseEntity = getAccountService.getError(INTEGER_EXCEEDED_LENGTH);
        assertBadRequestResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. GET '/account/{id}'. ID is zero")
    public void testZeroIdFailed() {
        var responseEntity = getAccountService.getError(ZERO_NUMBER);
        assertNotFoundResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. GET '/account/{id}'. ID - negative number")
    public void testNegativeIdFailed() {
        var responseEntity = getAccountService.getError(accountRandom.getNegativeNumber());
        assertNotFoundResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. GET '/account/{id}'. ID - non-numeric value with minimum length")
    public void testStringMinIdFailed() {
        var responseEntity = getAccountService.getError(accountRandom.getTextString(MIN_NUMBER));
        assertBadRequestResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. GET '/account/{id}'. ID - non-numeric value with maximum length")
    public void testStringMaxIdFailed() {
        var responseEntity = getAccountService.getError(accountRandom.getTextString(MAX_URL_LENGTH));
        assertBadRequestResponse(responseEntity);
    }
}