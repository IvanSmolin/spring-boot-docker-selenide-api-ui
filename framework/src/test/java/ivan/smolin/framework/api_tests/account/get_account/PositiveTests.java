package ivan.smolin.framework.api_tests.account.get_account;

import ivan.smolin.framework.api_tests.account.AccountApiBaseTestClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static ivan.smolin.framework.api.common.CustomHttpResponseAssertion.assertOkResponse;

@DisplayName("API. Account. GET '/account/{id}'. Positive tests of getting an account by ID")
public final class PositiveTests extends AccountApiBaseTestClass {
    @Test
    @DisplayName("API. Account. GET '/account/{id}'. Getting an account by ID")
    public void testSuccess() {
        var model = accountRepository.getRandomActiveAccount().toAccountModel();
        var responseEntity = getAccountService.get(model.getId());
        assertOkResponse(responseEntity, model);
    }
}