package ivan.smolin.framework.api_tests.account.post_account;

import ivan.smolin.framework.api.account.model.AccountModel;
import ivan.smolin.framework.api_tests.account.AccountApiBaseTestClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static ivan.smolin.framework.api.account.constants.AccountConstants.COUNTRY;
import static ivan.smolin.framework.api.account.constants.AccountConstants.COUNTRY_NAME;
import static ivan.smolin.framework.api.account.constants.AccountConstants.CURRENCY_NAME_MIN_LENGTH;
import static ivan.smolin.framework.api.account.constants.AccountConstants.EXCHANGE_RATES;
import static ivan.smolin.framework.api.account.constants.AccountConstants.NAME;
import static ivan.smolin.framework.api.account.constants.AccountConstants.NAME_MAX_LENGTH;
import static ivan.smolin.framework.api.account.constants.AccountConstants.NAME_MIN_LENGTH;
import static ivan.smolin.framework.api.common.CustomHttpResponseAssertion.assertResponseWithErrorMessages;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.fieldIsRequired;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.listOfObjectsIsRequired;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.maxLength;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.minLength;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.objectIsRequired;

@DisplayName("API. Account. POST '/account'. Negative account creation tests")
public final class NegativeTests extends AccountApiBaseTestClass {
    @Test
    @DisplayName("API. Account. POST '/account'. All attributes of the request body are null")
    public void testAttributesAreNullFailed() {
        var errorMap = new HashMap<String, String>() {{
            put(COUNTRY, objectIsRequired(COUNTRY));
            put(EXCHANGE_RATES, listOfObjectsIsRequired(EXCHANGE_RATES));
            put(NAME, fieldIsRequired(NAME));
        }};

        var responseEntity = postAccountService.postError(new AccountModel());
        assertResponseWithErrorMessages(responseEntity, errorMap);
    }

    @Test
    @DisplayName("API. Account. POST '/account'. Request body attributes have strings exceeding the maximum length")
    public void testAttributesExceededMaxLengthFailed() {
        var errorMap = new HashMap<String, String>() {{
            put(COUNTRY_NAME, maxLength(NAME_MAX_LENGTH));
            put(NAME, maxLength(NAME_MAX_LENGTH));
        }};

        var requestModel = accountRandom.getRandomModelExceeded();
        var responseEntity = postAccountService.postError(requestModel);
        assertResponseWithErrorMessages(responseEntity, errorMap);
    }

    @Test
    @DisplayName(
        "API. Account. POST '/account'. Attributes of the request body have a string length less than the minimum " +
            "length"
    )
    public void testAttributesLessMinLengthFailed() {
        var errorMap = new HashMap<String, String>() {{
            put(COUNTRY_NAME, minLength(NAME_MIN_LENGTH));
            put(NAME, minLength(CURRENCY_NAME_MIN_LENGTH));
        }};

        var requestModel = accountRandom.getRandomModelLessMin();
        var responseEntity = postAccountService.postError(requestModel);
        assertResponseWithErrorMessages(responseEntity, errorMap);
    }
}