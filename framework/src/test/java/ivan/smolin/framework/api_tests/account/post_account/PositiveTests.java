package ivan.smolin.framework.api_tests.account.post_account;

import ivan.smolin.framework.api.account.entity.AccountEntity;
import ivan.smolin.framework.api_tests.account.AccountApiBaseTestClass;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static ivan.smolin.framework.api.common.CustomHttpResponseAssertion.assertCreatedResponse;

@DisplayName("API. Account. POST '/account'. Positive account creation tests")
public final class PositiveTests extends AccountApiBaseTestClass {
    private AccountEntity entity;

    @Test
    @DisplayName("API. Account. POST '/account'. Creating an account with maximum data")
    public void testMaxBodySuccess() {
        var requestModel = accountRandom.getRandomModelMax();
        var responseEntity = postAccountService.post(requestModel);
        entity = accountRepository.findById(responseEntity.getBody().getId()).get();
        assertCreatedResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. POST '/account'. Creating an account with minimal data")
    public void testMinBodySuccess() {
        var requestModel = accountRandom.getRandomModelMin();
        var responseEntity = postAccountService.post(requestModel);
        entity = accountRepository.findById(responseEntity.getBody().getId()).get();
        assertCreatedResponse(responseEntity);
    }

    @AfterEach
    private void deleteTestValueFromDb() {
        accountRepository.delete(entity);
    }
}