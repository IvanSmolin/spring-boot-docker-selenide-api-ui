package ivan.smolin.framework.api_tests.currency;

import ivan.smolin.framework.api.currency.random.CurrencyRandom;
import ivan.smolin.framework.api.currency.repository.CurrencyRepository;
import ivan.smolin.framework.api.currency.service.DeleteCurrencyService;
import ivan.smolin.framework.api.currency.service.GetCurrencyService;
import ivan.smolin.framework.api.currency.service.PostCurrencyService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MAX_LIST_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.ZERO_NUMBER;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class CurrencyApiBaseTestClass {
    /**
     * Repositories
     */
    @Autowired
    protected CurrencyRepository currencyRepository;

    /**
     * Random Helpers
     */
    @Autowired
    protected CurrencyRandom currencyRandom;

    /**
     * Services
     */
    @Autowired
    protected PostCurrencyService postCurrencyService;

    @Autowired
    protected GetCurrencyService getCurrencyService;

    @Autowired
    protected DeleteCurrencyService deleteCurrencyService;

    @BeforeAll
    public void initializeCurrencyDatabase() {
        currencyRepository.saveAll(
            Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
                .limit(MAX_LIST_LENGTH)
                .map(it -> currencyRandom.getRandomEntity())
                .collect(Collectors.toList())
        );
    }
}