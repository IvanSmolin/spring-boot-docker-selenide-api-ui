package ivan.smolin.framework.api_tests.currency.delete_currency;

import ivan.smolin.framework.api_tests.currency.CurrencyApiBaseTestClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static ivan.smolin.framework.api.common.CustomHttpResponseAssertion.assertBadRequestStringResponse;
import static ivan.smolin.framework.api.common.CustomHttpResponseAssertion.assertNotFoundStringResponse;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.INTEGER_EXCEEDED_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.INTEGER_MAX_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MAX_URL_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.ZERO_NUMBER;

@DisplayName("API. Currency. DELETE '/currency/{id}'. Negative tests of currency deletion by ID")
public final class NegativeTests extends CurrencyApiBaseTestClass {
    @Test
    @DisplayName("API. Currency. DELETE '/currency/{id}'. Unknown ID")
    public void testUnknownIdFailed() {
        var responseEntity = deleteCurrencyService.delete(INTEGER_MAX_LENGTH);
        assertNotFoundStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. DELETE '/currency/{id}'. ID is greater than the boundary value for int type")
    public void testNotIntegerIdFailed() {
        var responseEntity = deleteCurrencyService.delete(INTEGER_EXCEEDED_LENGTH);
        assertBadRequestStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. DELETE '/currency/{id}'. ID is zero")
    public void testZeroIdFailed() {
        var responseEntity = deleteCurrencyService.delete(ZERO_NUMBER);
        assertNotFoundStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. DELETE '/currency/{id}'. ID - negative number")
    public void testNegativeIdFailed() {
        var responseEntity = deleteCurrencyService.delete(currencyRandom.getNegativeNumber());
        assertNotFoundStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. DELETE '/currency/{id}'. ID - non-numeric value with minimum length")
    public void testStringMinIdFailed() {
        var responseEntity = deleteCurrencyService.delete(currencyRandom.getTextString(MIN_NUMBER));
        assertBadRequestStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. DELETE '/currency/{id}'. ID - non-numeric value with maximum length")
    public void testStringMaxIdFailed() {
        var responseEntity = deleteCurrencyService.delete(currencyRandom.getTextString(MAX_URL_LENGTH));
        assertBadRequestStringResponse(responseEntity);
    }
}