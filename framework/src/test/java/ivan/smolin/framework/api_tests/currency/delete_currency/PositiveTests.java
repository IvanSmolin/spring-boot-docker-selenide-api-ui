package ivan.smolin.framework.api_tests.currency.delete_currency;

import ivan.smolin.framework.api_tests.currency.CurrencyApiBaseTestClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("API. Currency. DELETE '/currency/{id}'. Positive Currency deletion tests by ID")
public final class PositiveTests extends CurrencyApiBaseTestClass {
    @Test
    @DisplayName("API. Currency. DELETE '/currency/{id}'. Deleting currency by ID")
    public void testSuccess() {
        var entity = currencyRandom.getRandomEntity();
        currencyRepository.save(entity);
        var responseEntity = deleteCurrencyService.delete(entity.getId());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}