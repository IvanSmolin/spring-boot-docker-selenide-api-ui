package ivan.smolin.framework.api_tests.currency.get_currency;

import ivan.smolin.framework.api_tests.currency.CurrencyApiBaseTestClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static ivan.smolin.framework.api.common.CustomHttpResponseAssertion.assertBadRequestResponse;
import static ivan.smolin.framework.api.common.CustomHttpResponseAssertion.assertNotFoundResponse;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.INTEGER_EXCEEDED_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.INTEGER_MAX_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MAX_URL_LENGTH;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.ZERO_NUMBER;

@DisplayName("API. Currency. GET '/currency/{id}'. Negative tests of obtaining currency by ID")
public final class NegativeTests extends CurrencyApiBaseTestClass {
    @Test
    @DisplayName("API. Currency. GET '/currency/{id}'. Unknown ID")
    public void testUnknownIdFailed() {
        var responseEntity = getCurrencyService.getError(INTEGER_MAX_LENGTH);
        assertNotFoundResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. GET '/currency/{id}'. ID is greater than the boundary value for int type")
    public void testNotIntegerIdFailed() {
        var responseEntity = getCurrencyService.getError(INTEGER_EXCEEDED_LENGTH);
        assertBadRequestResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. GET '/currency/{id}'. ID is zero")
    public void testZeroIdFailed() {
        var responseEntity = getCurrencyService.getError(ZERO_NUMBER);
        assertNotFoundResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. GET '/currency/{id}'. ID - negative number")
    public void testNegativeIdFailed() {
        var responseEntity = getCurrencyService.getError(currencyRandom.getNegativeNumber());
        assertNotFoundResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. GET '/currency/{id}'. ID - non-numeric value with minimum length")
    public void testStringMinIdFailed() {
        var responseEntity = getCurrencyService.getError(currencyRandom.getTextString(MIN_NUMBER));
        assertBadRequestResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. GET '/currency/{id}'. ID - non-numeric value with maximum length")
    public void testStringMaxIdFailed() {
        var responseEntity = getCurrencyService.getError(currencyRandom.getTextString(MAX_URL_LENGTH));
        assertBadRequestResponse(responseEntity);
    }
}