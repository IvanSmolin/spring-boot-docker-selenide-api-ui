package ivan.smolin.framework.api_tests.currency.get_currency;

import ivan.smolin.framework.api_tests.currency.CurrencyApiBaseTestClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static ivan.smolin.framework.api.common.CustomHttpResponseAssertion.assertOkResponse;

@DisplayName("API. Currency. GET '/currency/{id}'. Positive tests for obtaining currency by ID")
public final class PositiveTests extends CurrencyApiBaseTestClass {
    @Test
    @DisplayName("API. Currency. GET '/currency/{id}'. Getting currency by ID")
    public void testSuccess() {
        var model = currencyRepository.getRandomActiveCurrency().toCurrencyModel();
        var responseEntity = getCurrencyService.get(model.getId());
        assertOkResponse(responseEntity, model);
    }
}