package ivan.smolin.framework.api_tests.currency.post_currency;

import ivan.smolin.framework.api.currency.model.CurrencyModel;
import ivan.smolin.framework.api_tests.currency.CurrencyApiBaseTestClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static ivan.smolin.framework.api.common.CustomHttpResponseAssertion.assertResponseWithErrorMessages;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.fieldIsRequired;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.listOfObjectsIsRequired;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.maxLength;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.minLength;
import static ivan.smolin.framework.api.common.constants.CommonApiConstants.objectIsRequired;
import static ivan.smolin.framework.api.currency.constants.CurrencyConstants.COUNTRY;
import static ivan.smolin.framework.api.currency.constants.CurrencyConstants.COUNTRY_NAME;
import static ivan.smolin.framework.api.currency.constants.CurrencyConstants.CURRENCY_NAME_MIN_LENGTH;
import static ivan.smolin.framework.api.currency.constants.CurrencyConstants.EXCHANGE_RATES;
import static ivan.smolin.framework.api.currency.constants.CurrencyConstants.NAME;
import static ivan.smolin.framework.api.currency.constants.CurrencyConstants.NAME_MAX_LENGTH;
import static ivan.smolin.framework.api.currency.constants.CurrencyConstants.NAME_MIN_LENGTH;

@DisplayName("API. Currency. POST '/currency'. Negative tests of currency creation")
public final class NegativeTests extends CurrencyApiBaseTestClass {
    @Test
    @DisplayName("API. Currency. POST '/currency'. All attributes of the request body are null")
    public void testAttributesAreNullFailed() {
        var errorMap = new HashMap<String, String>() {{
            put(COUNTRY, objectIsRequired(COUNTRY));
            put(EXCHANGE_RATES, listOfObjectsIsRequired(EXCHANGE_RATES));
            put(NAME, fieldIsRequired(NAME));
        }};

        var responseEntity = postCurrencyService.postError(new CurrencyModel());
        assertResponseWithErrorMessages(responseEntity, errorMap);
    }

    @Test
    @DisplayName("API. Currency. POST '/currency'. Request body attributes have strings exceeding the maximum length")
    public void testAttributesExceededMaxLengthFailed() {
        var errorMap = new HashMap<String, String>() {{
            put(COUNTRY_NAME, maxLength(NAME_MAX_LENGTH));
            put(NAME, maxLength(NAME_MAX_LENGTH));
        }};

        var requestModel = currencyRandom.getRandomModelExceeded();
        var responseEntity = postCurrencyService.postError(requestModel);
        assertResponseWithErrorMessages(responseEntity, errorMap);
    }

    @Test
    @DisplayName(
        "API. Currency. POST '/currency'. The attributes of the request body have a string length less than the " +
            "minimum length"
    )
    public void testAttributesLessMinLengthFailed() {
        var errorMap = new HashMap<String, String>() {{
            put(COUNTRY_NAME, minLength(NAME_MIN_LENGTH));
            put(NAME, minLength(CURRENCY_NAME_MIN_LENGTH));
        }};

        var requestModel = currencyRandom.getRandomModelLessMin();
        var responseEntity = postCurrencyService.postError(requestModel);
        assertResponseWithErrorMessages(responseEntity, errorMap);
    }
}