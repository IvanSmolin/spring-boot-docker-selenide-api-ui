package ivan.smolin.framework.api_tests.currency.post_currency;

import ivan.smolin.framework.api.currency.entity.CurrencyEntity;
import ivan.smolin.framework.api.currency.model.CurrencyResponseModel;
import ivan.smolin.framework.api_tests.currency.CurrencyApiBaseTestClass;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static ivan.smolin.framework.api.common.CustomHttpResponseAssertion.assertCreatedResponse;

@DisplayName("API. Currency. POST '/currency'. Positive tests of currency creation")
public final class PositiveTests extends CurrencyApiBaseTestClass {
    private CurrencyEntity entity;

    @Test
    @DisplayName("API. Currency. POST '/currency'. Creating a currency with maximum data")
    public void testMaxBodySuccess() {
        var requestModel = currencyRandom.getRandomModelMax();
        ResponseEntity<CurrencyResponseModel> responseEntity = postCurrencyService.post(requestModel);
        entity = currencyRepository.findById(responseEntity.getBody().getId()).get();
        assertCreatedResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. POST '/currency'. Creating a currency with minimal data")
    public void testMinBodySuccess() {
        var requestModel = currencyRandom.getRandomModelMin();
        ResponseEntity<CurrencyResponseModel> responseEntity = postCurrencyService.post(requestModel);
        entity = currencyRepository.findById(responseEntity.getBody().getId()).get();
        assertCreatedResponse(responseEntity);
    }

    @AfterEach
    private void deleteTestValueFromDb() {
        currencyRepository.delete(entity);
    }
}