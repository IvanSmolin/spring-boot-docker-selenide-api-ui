package ivan.smolin.framework.ui_tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import ivan.smolin.framework.ui.common.CustomProvider;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public abstract class BaseUiTestClass {
    @BeforeEach
    public void setUpBrowser() {
        Configuration.driverManagerEnabled = false;
        Configuration.browser = CustomProvider.class.getName();

        Configuration.startMaximized = true;
        Configuration.screenshots = false;
        Configuration.fastSetValue = true;
        Configuration.timeout = 10000;
        Configuration.headless = true;

        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
    }

    @AfterEach
    public void tearDownBrowser() {
        SelenideLogger.removeListener("AllureSelenide");
        Selenide.closeWebDriver();
    }
}