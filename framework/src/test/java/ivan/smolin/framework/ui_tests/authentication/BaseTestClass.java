package ivan.smolin.framework.ui_tests.authentication;

import com.codeborne.selenide.Selenide;
import ivan.smolin.framework.api.user.random.UserRandom;
import ivan.smolin.framework.ui.authentication.page_object.LoginPage;
import ivan.smolin.framework.ui.authentication.resources.LoginPageResources;
import ivan.smolin.framework.ui_tests.BaseUiTestClass;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public abstract class BaseTestClass extends BaseUiTestClass {
    @Autowired
    protected LoginPage loginPage;

    @Autowired
    protected UserRandom userRandom;

    @Autowired
    protected LoginPageResources loginPageResources;

    @Value("${uiUrl.authentication}")
    private String pageUrl;

    @BeforeEach
    protected void setUp() {
        Selenide.open(pageUrl);
    }
}