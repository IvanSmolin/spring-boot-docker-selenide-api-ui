package ivan.smolin.framework.ui_tests.authentication;

import ivan.smolin.framework.api.user.entity.UserEntity;
import ivan.smolin.framework.ui.common.component.Label;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@Disabled
@DisplayName("UI. Login. Positive tests")
public final class PositiveTests extends BaseTestClass {
    private UserEntity entity;

    @Test
    @DisplayName("UI. Login. The user successfully logs in")
    public void testLoginSuccess() {
        loginPage.register();
        new Label(loginPageResources.getWelcomeText()).shouldBeVisible();
    }

    @Test
    @DisplayName("UI. Login. The user successfully creates an account")
    public void testCreateAccountSuccess() {
        UserEntity randomEntity = userRandom.getRandomEntity();
        loginPage.createAccount(randomEntity)
            .fillPersonalInformation(randomEntity)
            .fillAddressModule(randomEntity)
            .send();

//        Code for checking the recorded value in the database, for example:
//        entity = userRepository.findByEmail(randomEntity.getEmail());
//        assertThat(randomEntity).isEqualTo(entity);
    }

    @AfterEach
    private void deleteTestValueFromDb() {
//        Code for clearing entities from the database, for example:
//        userRepository.delete(entity);
    }
}