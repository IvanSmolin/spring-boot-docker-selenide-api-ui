# Account test service created for the webinar

This project contains a service created to be shown at the webinar

Integration API tests for the service are implemented in the following
project: https://gitlab.com/IvanSmolin/webinar-test-framework

The service is written in Java based on the Spring Boot Framework, which implements dependency
injection mechanisms, obtaining the necessary properties from yml files, working with databases and much more (for more
information, see the official developer's website: https://spring.io/projects/spring-boot)

#### Used libraries

* Spring Boot is used to process REST API requests
* Spring Data JPA is used to work with the database
* The postgresql driver is used to connect to the database
* Lombok is used for automatic generation of geters, seters and constructors
* H2 database is used for local development and testing
* To check the service coverage with tests, the JaCoCo plugin is used
* AssertJ is used to verify the received data in the tests
* JUnit5 is used to implement and run tests
* Spring Profiles is used to select the startup configuration

#### Project structure

The framework has two root sources:

* main.java.ivan.smolin.framework
* test.java.ivan.smolin.framework

"main.java.ivan.smolin.account" root has the following structure:

* Package "**controller**" (contains controllers for processing REST requests of the service)
* Package "**dto**" (contains data transfer object classes for working with service data)
* Package "**entity**" (contains data access object entity classes for working with the database)
* Package "**exception**" (contains classes for handling exceptions while working with the API)
* Package "**repository**" (contains interfaces for working with databases via Spring Data JPA)
* Package "**service**" (contains classes implementing methods for working with the API)
* Class "**AccountApplication**" (contains the main method that implements entry point of the service)
* Resource root "**resources**" (contains yml configuration files with the data necessary to connect to the database and
  select the method of launching the service using Spring Profiles)

"test.java.ivan.smolin.account" root has the following structure:

* Package "**controller**" (tests are stored to check service controllers)
* Package "**service**" (tests are stored to check the services of the service)

The file "**.gitignore**" stores the names of packages that will not be added to commits when pushing changes to the
GitLab repository

In the file "**build.gradle**" describes plugins and libraries that are used in the service

In the file "**docker-compose.yml**" describes the configuration for launching Docker containers with the database and
the service itself

The file "**Dockerfile**" describes a script for copying a jar file with the assembled service into a container for
automatically starting the service

#### Swagger

Swagger documentation for the service is available in the following
repository: https://gitlab.com/IvanSmolin/webinar-swagger

#### Building a project

Gradle is used to build the project.

The project implements the use of Gradle Wrapper, so when cloning the repository, there is no need to have Gradle
installed on the local machine.

All commands in the terminal must be run via `./gradlew`

#### Choosing a startup configuration

The service has 2 launch profiles:

* **dev** - designed for local launch of containers in Docker for the database and by the service itself
* **test** - designed for local launch of the service with H2 database

To select a specific profile in the settings, follow the path:

`"Run/Debug Configuration" -> "Edit configuration templates..." -> "Gradle" -> "Environment variables"`

Add the value `PROFILE=dev` or `PROFILE=test`

#### Running the service in Docker locally

You need to install Docker Desktop locally:
https://www.docker.com/products/docker-desktop

To start the service, use the command in the terminal:

* `./gradlew clean build` (only if it is started for the first time)
* `docker-compose up`

To disable it, use the command:

* `docker-compose down`

When changing the code, in order for the changes to be applied in the service, it is necessary:

* `docker image rm account` - to delete the current docker image of the service
* `./gradlew clean build` - to build the service code

The service is available at: http://localhost:8084/account

#### Running a service with an H2 database locally

To start the service, you need to run the main function in the AccountApplication class

#### Running tests via the terminal

To run Unit tests of the service, use the command in the terminal:

* `./gradlew clean test --tests "ivan.smolin.account.*"`