package ivan.smolin.account.controller;

import ivan.smolin.account.entity.AccountEntity;
import ivan.smolin.account.model.AccountResponse;
import ivan.smolin.account.service.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/account")
public class AccountController {
    private final static String ID = "id";

    private final AccountService service;

    public AccountController(AccountService service) {
        this.service = service;
    }

    @GetMapping()
    public ResponseEntity<List<AccountEntity>> getAllAccounts() {
        return ResponseEntity.ok().body(service.getAll());
    }

    @GetMapping("/{" + ID + "}")
    public ResponseEntity<AccountEntity> getAccountById(@PathVariable(ID) int id) {
        var entity = service.getById(id);
        return ResponseEntity.ok().body(entity);
    }

    @PostMapping()
    public ResponseEntity<AccountResponse> createAccount(@Valid @RequestBody AccountEntity entity) {
        var dto = service.create(entity);
        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }

    @DeleteMapping("/{" + ID + "}")
    public ResponseEntity deleteAccount(@PathVariable(ID) int id) {
        service.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}