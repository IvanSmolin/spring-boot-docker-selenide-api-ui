package ivan.smolin.account.entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "account")
public class AccountEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    @NotBlank(message = "Поле \"name\" обязательное")
    @Length(min = 2, message = "Минимальная длина 2 символов")
    @Length(max = 100, message = "Максимальная длина 100 символов")
    private String name;

    @Column(name = "created_at")
    @LastModifiedDate
    private LocalDateTime created_at = LocalDateTime.now().withNano(0);

    @Column(name = "active")
    private boolean active = true;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    @NotNull(message = "Список объектов \"exchangeRates\" должен быть заполнен")
    private List<@Valid AccountExchangeRateEntity> exchangeRates = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_country_id")
    @NotNull(message = "Объект \"country\" должен быть заполнен")
    private @Valid AccountCountryEntity country;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
        name = "account_extra_map",
        joinColumns = {@JoinColumn(name = "account_id")},
        inverseJoinColumns = {@JoinColumn(name = "extra_id")}
    )
    private List<AccountExtraEntity> extras = new ArrayList<>();
}