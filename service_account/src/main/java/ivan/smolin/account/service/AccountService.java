package ivan.smolin.account.service;

import ivan.smolin.account.entity.AccountEntity;
import ivan.smolin.account.model.AccountResponse;

import java.util.List;

public interface AccountService {
    List<AccountEntity> getAll();

    AccountEntity getById(int id);

    AccountResponse create(AccountEntity entity);

    void deleteById(int id);
}