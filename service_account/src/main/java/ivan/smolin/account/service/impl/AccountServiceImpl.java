package ivan.smolin.account.service.impl;

import ivan.smolin.account.entity.AccountEntity;
import ivan.smolin.account.model.AccountResponse;
import ivan.smolin.account.repository.AccountRepository;
import ivan.smolin.account.service.AccountService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {
    private final AccountRepository repository;

    public AccountServiceImpl(AccountRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<AccountEntity> getAll() {
        return repository.findAll();
    }

    @Override
    public AccountEntity getById(int id) {
        return repository.findById(id).get();
    }

    @Override
    public AccountResponse create(AccountEntity entity) {
        repository.save(entity);
        return new AccountResponse(entity.getId());
    }

    @Override
    public void deleteById(int id) {
        repository.deleteById(id);
    }
}