package ivan.smolin.account.controller;

import ivan.smolin.account.entity.AccountEntity;
import ivan.smolin.account.model.AccountResponse;
import ivan.smolin.account.service.AccountService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountControllerTest {
    private final static AccountEntity ENTITY = new AccountEntity();

    @Mock
    private AccountService accountService;

    @InjectMocks
    private AccountController accountController;

    @Test
    public void getByIdReturnsValidResponse() {
        when(accountService.getById(1)).thenReturn(ENTITY);
        assertEquals(new ResponseEntity<>(ENTITY, HttpStatus.OK), accountController.getAccountById(1));
    }

    @Test
    public void getAllReturnsValidResponse() {
        when(accountService.getAll()).thenReturn(Collections.singletonList(ENTITY));
        assertEquals(
            new ResponseEntity<>(Collections.singletonList(ENTITY), HttpStatus.OK),
            accountController.getAllAccounts()
        );
    }

    @Test
    public void createReturnsValidResponse() {
        when(accountService.create(ENTITY)).thenReturn(new AccountResponse(1));
        assertEquals(
            new ResponseEntity<>(new AccountResponse(1), HttpStatus.CREATED),
            accountController.createAccount(ENTITY)
        );
    }

    @Test
    public void deleteByIdReturnsValidResponse() {
        assertEquals(new ResponseEntity<>(HttpStatus.OK), accountController.deleteAccount(1));
        verify(accountService).deleteById(1);
    }
}