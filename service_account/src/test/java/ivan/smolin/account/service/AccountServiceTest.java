package ivan.smolin.account.service;

import ivan.smolin.account.entity.AccountEntity;
import ivan.smolin.account.model.AccountResponse;
import ivan.smolin.account.repository.AccountRepository;
import ivan.smolin.account.service.impl.AccountServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {
    private final static AccountEntity ENTITY = new AccountEntity();

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private AccountServiceImpl accountService;

    @Test
    public void getByIdReturnsValidResponse() {
        when(accountRepository.findById(1)).thenReturn(Optional.of(ENTITY));
        assertEquals(ENTITY, accountService.getById(1));
    }

    @Test
    public void getAllReturnsValidResponse() {
        when(accountRepository.findAll()).thenReturn(Collections.singletonList(ENTITY));
        assertEquals(Collections.singletonList(ENTITY), accountService.getAll());
    }

    @Test
    public void createReturnsValidResponse() {
        when(accountRepository.save(ENTITY)).thenReturn(ENTITY);
        assertEquals(new AccountResponse(0), accountService.create(ENTITY));
    }

    @Test
    public void deleteByIdReturnsValidResponse() {
        accountService.deleteById(1);
        verify(accountRepository).deleteById(1);
    }
}