package ivan.smolin.currency.controller;

import ivan.smolin.currency.entity.CurrencyEntity;
import ivan.smolin.currency.model.CurrencyResponse;
import ivan.smolin.currency.service.CurrencyService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/currency")
public class CurrencyController {
    private final static String ID = "id";

    private final CurrencyService service;

    public CurrencyController(CurrencyService service) {
        this.service = service;
    }

    @GetMapping()
    public ResponseEntity<List<CurrencyEntity>> getAllCurrencies() {
        return ResponseEntity.ok().body(service.getAll());
    }

    @GetMapping("/{" + ID + "}")
    public ResponseEntity<CurrencyEntity> getCurrencyById(@PathVariable(ID) int id) {
        var entity = service.getById(id);
        return ResponseEntity.ok().body(entity);
    }

    @PostMapping()
    public ResponseEntity<CurrencyResponse> createCurrency(@Valid @RequestBody CurrencyEntity entity) {
        var dto = service.create(entity);
        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }

    @DeleteMapping("/{" + ID + "}")
    public ResponseEntity deleteCurrency(@PathVariable(ID) int id) {
        service.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}