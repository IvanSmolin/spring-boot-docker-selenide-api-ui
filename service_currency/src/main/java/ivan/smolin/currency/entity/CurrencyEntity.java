package ivan.smolin.currency.entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "currency")
public class CurrencyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    @NotBlank(message = "Поле \"name\" обязательное")
    @Length(min = 2, message = "Минимальная длина 2 символов")
    @Length(max = 100, message = "Максимальная длина 100 символов")
    private String name;

    @Column(name = "created_at")
    @LastModifiedDate
    private LocalDateTime created_at = LocalDateTime.now().withNano(0);

    @Column(name = "active")
    private boolean active = true;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "currency_id", referencedColumnName = "id")
    @NotNull(message = "Список объектов \"exchangeRates\" должен быть заполнен")
    private List<@Valid CurrencyExchangeRateEntity> exchangeRates = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "currency_country_id")
    @NotNull(message = "Объект \"country\" должен быть заполнен")
    private @Valid CurrencyCountryEntity country;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
        name = "currency_extra_map",
        joinColumns = {@JoinColumn(name = "currency_id")},
        inverseJoinColumns = {@JoinColumn(name = "extra_id")}
    )
    private List<CurrencyExtraEntity> extras = new ArrayList<>();
}