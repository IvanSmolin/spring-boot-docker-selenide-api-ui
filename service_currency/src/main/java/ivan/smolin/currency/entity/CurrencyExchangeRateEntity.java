package ivan.smolin.currency.entity;

import lombok.Data;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "currency_exchange_rate")
public class CurrencyExchangeRateEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "rate")
    @NotNull(message = "Поле \"rate\" обязательное")
    private Double rate;

    @Column(name = "rate_date")
    private LocalDate rate_date = LocalDate.now();

    @Column(name = "created_at")
    @LastModifiedDate
    private LocalDateTime created_at = LocalDateTime.now().withNano(0);
}