package ivan.smolin.currency.entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "currency_extra")
public class CurrencyExtraEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    @NotBlank(message = "Поле \"name\" обязательное")
    @Length(min = 5, message = "Минимальная длина 2 символа")
    @Length(max = 100, message = "Максимальная длина 100 символов")
    private String name;

    @Column(name = "created_at")
    @LastModifiedDate
    private LocalDateTime created_at = LocalDateTime.now().withNano(0);
}