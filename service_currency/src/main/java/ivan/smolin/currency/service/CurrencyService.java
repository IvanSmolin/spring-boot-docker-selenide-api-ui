package ivan.smolin.currency.service;

import ivan.smolin.currency.entity.CurrencyEntity;
import ivan.smolin.currency.model.CurrencyResponse;

import java.util.List;

public interface CurrencyService {
    List<CurrencyEntity> getAll();

    CurrencyEntity getById(int id);

    CurrencyResponse create(CurrencyEntity entity);

    void deleteById(int id);
}