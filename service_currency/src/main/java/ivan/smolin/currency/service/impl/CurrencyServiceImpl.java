package ivan.smolin.currency.service.impl;

import ivan.smolin.currency.entity.CurrencyEntity;
import ivan.smolin.currency.model.CurrencyResponse;
import ivan.smolin.currency.repository.CurrencyRepository;
import ivan.smolin.currency.service.CurrencyService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurrencyServiceImpl implements CurrencyService {
    private final CurrencyRepository repository;

    public CurrencyServiceImpl(CurrencyRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<CurrencyEntity> getAll() {
        return repository.findAll();
    }

    @Override
    public CurrencyEntity getById(int id) {
        return repository.findById(id).get();
    }

    @Override
    public CurrencyResponse create(CurrencyEntity entity) {
        repository.save(entity);
        return new CurrencyResponse(entity.getId());
    }

    @Override
    public void deleteById(int id) {
        repository.deleteById(id);
    }
}