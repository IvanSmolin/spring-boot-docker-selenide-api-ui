package ivan.smolin.currency.controller;

import ivan.smolin.currency.entity.CurrencyEntity;
import ivan.smolin.currency.model.CurrencyResponse;
import ivan.smolin.currency.service.CurrencyService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CurrencyControllerTest {
    private final static CurrencyEntity ENTITY = new CurrencyEntity();

    @Mock
    private CurrencyService currencyService;

    @InjectMocks
    private CurrencyController currencyController;

    @Test
    public void getByIdReturnsValidResponse() {
        when(currencyService.getById(1)).thenReturn(ENTITY);
        assertEquals(new ResponseEntity<>(ENTITY, HttpStatus.OK), currencyController.getCurrencyById(1));
    }

    @Test
    public void getAllReturnsValidResponse() {
        when(currencyService.getAll()).thenReturn(Collections.singletonList(ENTITY));
        assertEquals(
            new ResponseEntity<>(Collections.singletonList(ENTITY), HttpStatus.OK),
            currencyController.getAllCurrencies()
        );
    }

    @Test
    public void createReturnsValidResponse() {
        when(currencyService.create(ENTITY)).thenReturn(new CurrencyResponse(1));
        assertEquals(
            new ResponseEntity<>(new CurrencyResponse(1), HttpStatus.CREATED),
            currencyController.createCurrency(ENTITY)
        );
    }

    @Test
    public void deleteByIdReturnsValidResponse() {
        assertEquals(new ResponseEntity<>(HttpStatus.OK), currencyController.deleteCurrency(1));
        verify(currencyService).deleteById(1);
    }
}