package ivan.smolin.currency.service;

import ivan.smolin.currency.entity.CurrencyEntity;
import ivan.smolin.currency.model.CurrencyResponse;
import ivan.smolin.currency.repository.CurrencyRepository;
import ivan.smolin.currency.service.impl.CurrencyServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CurrencyServiceTest {
    private final static CurrencyEntity ENTITY = new CurrencyEntity();

    @Mock
    private CurrencyRepository currencyRepository;

    @InjectMocks
    private CurrencyServiceImpl currencyService;

    @Test
    public void getByIdReturnsValidResponse() {
        when(currencyRepository.findById(1)).thenReturn(Optional.of(ENTITY));
        assertEquals(ENTITY, currencyService.getById(1));
    }

    @Test
    public void getAllReturnsValidResponse() {
        when(currencyRepository.findAll()).thenReturn(Collections.singletonList(ENTITY));
        assertEquals(Collections.singletonList(ENTITY), currencyService.getAll());
    }

    @Test
    public void createReturnsValidResponse() {
        when(currencyRepository.save(ENTITY)).thenReturn(ENTITY);
        assertEquals(new CurrencyResponse(0), currencyService.create(ENTITY));
    }

    @Test
    public void deleteByIdReturnsValidResponse() {
        currencyService.deleteById(1);
        verify(currencyRepository).deleteById(1);
    }
}