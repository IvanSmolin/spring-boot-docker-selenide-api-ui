# Swagger documentation

This Swagger documentation was created to be shown at the webinar

The services described in the documentation are implemented in the following projects:

* https://gitlab.com/IvanSmolin/webinar-test-service-currency
* https://gitlab.com/IvanSmolin/webinar-test-service-account

The documentation is written in yml files

#### Running documentation in Docker locally

You need to install Docker Desktop locally:
https://www.docker.com/products/docker-desktop

To run the documentation, use the command in the terminal:

* `docker-compose up`

To disable it, use the command:

* `docker-compose down`

Documentation is available at: http://localhost:9090/